package com.miki.set.jpa.object;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: SetResult
 *
 */
@Entity
public class StockResult implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	private Long Id;
	
	private String stockIdx;
	private Long stockLast;   


	public StockResult() {
		super();
	}   
	public String getStockIdx() {
		return this.stockIdx;
	}

	public void setStockIdx(String stockIdx) {
		this.stockIdx = stockIdx;
	}   
	public Long getStockLast() {
		return this.stockLast;
	}

	public void setStockLast(Long stockLast) {
		this.stockLast = stockLast;
	}   
	public Long getId() {
		return this.Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}
   
}
